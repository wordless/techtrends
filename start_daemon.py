from simserver import SessionServer
from config import Config
import gensim
import logging


logging.basicConfig(format=Config.LOG_FORMAT, level=Config.LOG_LEVEL)


def main():

    server = SessionServer(Config.SIMILARITY_SERVER)
    gensim.utils.pyro_daemon('gensim.server', server)

if __name__ == '__main__':
    main()
