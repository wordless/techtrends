from WebServer import app
from config import Config
import logging


logging.basicConfig(format=Config.LOG_FORMAT, level=Config.LOG_LEVEL)


if __name__ == '__main__':

    app.run(debug=Config.DEBUG)
