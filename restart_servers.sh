#!/bin/sh

. /root/techtrends_env/bin/activate
python /root/techtrends_env/techtrends/update_config.py
python /root/techtrends_env/techtrends/train_server.py

kill $(ps aux | grep '[p]ython .*start_webserver.py' | awk '{print $2}')
kill $(ps aux | grep '[p]ython .*start_daemon.py' | awk '{print $2}')

sleep 3
python -m Pyro4.naming -n 0.0.0.0 &
sleep 3
python /root/techtrends_env/techtrends/start_daemon.py &
sleep 3
python /root/techtrends_env/techtrends/start_webserver.py &
