from SimilarityServer import save_topics
from datetime import date, timedelta
from config import Config
import logging


logging.basicConfig(format=Config.LOG_FORMAT, level=Config.LOG_LEVEL)


def main():

    start = date(2013, 4, 1)
    end = start + timedelta(days=14)
    save_topics(start, end, 5)

if __name__ == '__main__':

    main()
