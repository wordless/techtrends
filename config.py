import logging


class Config:
    DB_FILE = ''
    SIMILARITY_SERVER = ''
    CACHE = ''
    LOG_FORMAT = '%(asctime)s : %(levelname)s : %(message)s'
    LOG_LEVEL = logging.INFO
    DEBUG = False
