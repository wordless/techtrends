import re
_BLACKLISTED_EXPRESSIONS = [
    re.compile(r'\d+ points by \w+ \d+ minutes ago \| \d+ comments'),
    re.compile(r'\w+ \d+ (minutes|hours|days) ago \s+\|\s+link')
]


def delete_blacklisted(text):
    """
        Delete blacklisted words and text fragments.
        Edit this file if you wish to blacklist more
        words and fragments
    """
    for regex in _BLACKLISTED_EXPRESSIONS:
        text = re.sub(regex, '', text)

    return text
