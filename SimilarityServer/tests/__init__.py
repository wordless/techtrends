import unittest
import arc90_test
import preprocess_test


def run():
    suite = unittest.TestSuite()
    for method in dir(arc90_test.Test):
        if method.startswith("test"):
            suite.addTest(arc90_test.Test(method))
    for method in dir(preprocess_test.Test):
        if method.startswith("test"):
            suite.addTest(preprocess_test.Test(method))
    runner = unittest.TextTestRunner()
    runner.run(suite)
