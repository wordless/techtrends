from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize
from db import linkids_with_html, fetch_link
from contextlib import closing
from config import Config
from blacklist import delete_blacklisted
import arc90
import anydbm
import logging
import requests
import zlib

BOILERPIPE_AVAILABE = False
try:
    from boilerpipe.extract import Extractor
    BOILERPIPE_AVAILABE = True
except:
    print 'boilerpipe not availabe, using arc90'


STOP_WORDS = set(stopwords.words('english'))


def _decompress(buff):
    return zlib.decompress(buff).decode('utf-8')


def _to_word_list(text, min_word_length=2):

    """
        Converts a string to a list of words, e.g. ["one", "two"...]
        that is lemmatized, lowered and cleaned from stop words.

        min_word_length defines the minimum length of a word so that
        it will not be removed from the list
    """

    lemmatizer = WordNetLemmatizer()
    words = wordpunct_tokenize(text.lower())
    return [
        lemmatizer.lemmatize(word) for word in words
        if len(word) > min_word_length and
        word not in STOP_WORDS and
        not word.isdigit() and
        word.isalnum()
    ]


def _save_corpus(corpus, failed_ids):

    with closing(anydbm.open(Config.CACHE, 'c')) as cache:

        for document in corpus:
            word_list = ','.join(document['tokens']).encode('utf-8')
            cache[str(document['id'])] = word_list

        cache['failed_ids'] = ','.join(map(str, failed_ids))


def _load_corpus():

    with closing(anydbm.open(Config.CACHE, 'c')) as cache:

        corpus = []

        for id, word_list in cache.iteritems():
            word_list = word_list.decode('utf-8').split(',')
            if not id == 'failed_ids':
                corpus.append({
                    'id': int(id),
                    'tokens': word_list
                })

        failed_ids = cache['failed_ids'].split(',') if 'failed_ids' in cache else []

        return corpus, set(map(int, failed_ids))


def _create_document(id, min_token_length=20):

    try:
        link = fetch_link(id)

        if link['url'].find('twitter.com') != -1:
            return None

        if link['url'].find('youtube.com/watch') != -1:
            return None

        if BOILERPIPE_AVAILABE:
            extractor = Extractor(
                extractor='ArticleExtractor',
                url='http://localhost:5001/%s' % id
            )
            partial_html = extractor.getHTML()
            text = arc90.render_html(partial_html)
            text = delete_blacklisted(text)
            tokens = _to_word_list(text)
            if len(tokens) < min_token_length:
                return None
        else:
            html = _decompress(link['html'])
            tokens = _to_word_list(arc90.extract_content(html))
        if tokens:
            return {
                'id': id,
                'tokens': tokens
            }
    except:
        logging.debug('Could not preprocess document with id %s', id)


def create_corpus():

    corpus, failed_ids = _load_corpus()
    logging.info('Loaded cached corpus with %s documents', len(corpus))
    cached_ids = set([doc['id'] for doc in corpus])

    ids = set(linkids_with_html())
    ids = (ids - cached_ids) - failed_ids
    logging.info('Preprocessing %s new documents', len(ids))

    new_corpus = filter(None, [_create_document(id) for id in ids])

    successful_ids = set([doc['id'] for doc in new_corpus])
    logging.info('Preprocessing successful for %s documents', len(successful_ids))

    failed_ids.update(ids - successful_ids)
    corpus.extend(new_corpus)

    logging.info('Caching %s documents', len(corpus))
    _save_corpus(corpus, failed_ids)

    return corpus


def link_to_tokens(link):
    '''
        Takes a link, retrieves its content via Arc90 and creates a
        lemmatized (etc.) list of word from the content.
    '''

    if BOILERPIPE_AVAILABE:
        extractor = Extractor(
            extractor='ArticleExtractor',
            url=link
        )
        return _to_word_list(extractor.getText())
    else:
        request = requests.get(link)
        content = arc90.extract_content(request.text)
        return _to_word_list(content)
