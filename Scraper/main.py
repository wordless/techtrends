from time import time
from scraper import crawl_hackernews, crawl_reddit, fetch_html_for_posts
import db


def run_scraper(update=False):

    now = time()
    time_stamps = dict(first=now, last=now)

    # set current time to all posts
    # column first wont be updated in the database if the entry already exists
    try:
        hackernews_posts = [
            dict(post, **time_stamps) for post in crawl_hackernews()
        ]
        db.insert_hackernews_posts(hackernews_posts)
    except:
        print 'could not fetch hackernews'

    try:
        reddit_posts = [dict(post, **time_stamps) for post in crawl_reddit()]
        db.insert_reddit_posts(reddit_posts)
    except:
        print 'could not fetch reddit'

    # updates the entries with missing html content
    if update:
        db.update_links(fetch_html_for_posts(db.get_links_where_html_absent()))
