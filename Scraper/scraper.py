import zlib
import requests
import logger as log
from bs4 import BeautifulSoup
from werkzeug.utils import unescape


HN_URL = 'https://news.ycombinator.com'
IHACKERNEWS_URL = 'http://api.ihackernews.com/page'
REDDIT_URL = 'http://reddit.com/r/programming'


_BLACKLIST = [
    'imgur.com',
    'online.wsj.com'
]


def _is_blacklisted_url(url):

    for domain in _BLACKLIST:
        if url.find(domain) != -1:
            return True

    return False


def _is_invalid_filetype(url):
    """
        Returns True if the url points to an image or pdf
    """
    return (
        url.endswith('.pdf') or url.endswith('.png') or url.endswith('.jpg') or url.endswith('.gif')
    )


def _zip_html(html):
    return buffer(zlib.compress(html.encode('utf-8')))


def _fetch_html(url):
    """Fetch html for a given url and return as string
        Returns None if html could not be fetched
    """
    try:
        request = requests.get(url)
    except Exception:
        log.getFileLogger().exception("HTML fetching didn't work for %s" % url)
        return None

    if (request.headers['content-type'].find('text/html') == -1 and
            url.find('news.ycombinator.com') == -1):
        raise Exception('invalid content type %s' % request.headers['content-type'])

    if request.status_code == 200:
        return request.text
    else:
        log.getFileLogger().exception("Bad response code for %s" % url)
        return None


def _parse_hackernews(html):

    posts = []

    soup = BeautifulSoup(html)

    main_table = soup.find_all('table')[2]
    rows = main_table.find_all('tr')
    # always two rows represent a post, group them within a tupel
    # every third index is a filler row and contains no content
    # it will be ignored
    articles = zip(rows[::3], rows[1::3])
    # remove the last item which is the 'more' link
    articles.pop()

    for article in articles:
        try:
            link = article[0].find_all('a')[1]
            url = link['href']
            #dont save images and pdfs
            if _is_invalid_filetype(url) or _is_blacklisted_url(url):
                continue

            #fix links for posts on hackernews url
            if url.startswith('item?id='):
                url = 'https://news.ycombinator.com/%s' % url
            title = link.text
            #subtext contains link to comments and number of votes
            subtext = article[1].find_all('td', {'class': 'subtext'})[0]
            unique_id = subtext.span['id'].split('_')[1]
            comment_link = subtext.find_all('a')[1]
            votes = subtext.span.text.split()[0]
            # if the string starts with discuss,
            # then no one has commented yet
            if comment_link.text.startswith('discuss'):
                comments = 0
            else:
                comments = comment_link.text.split()[0]

            html = _fetch_html(url)
            zipped_html = _zip_html(html) if html else None

            posts.append(dict(
                unique_id=unique_id,
                url=url,
                html=zipped_html,
                title=title,
                votes=int(votes),
                comments=int(comments)
            ))
        except Exception:
            log.getFileLogger().exception('Could not parse article: %s' % (article,))

    return posts


def _parse_ihackernews_api(json):

    posts = []
    for item in json['items']:

        if item['url'].startswith('/comment'):
            continue

        html = _fetch_html(unescape(item['url']))
        zipped_html = _zip_html(html) if html else None

        posts.append(dict(
            unique_id=item['id'],
            url=unescape(item['url']),
            html=zipped_html,
            title=item['title'],
            votes=item['points'],
            comments=item['commentCount']
        ))

    return posts


def _parse_reddit(html):

    posts = []

    soup = BeautifulSoup(html)

    articles = soup.find_all('div', {'class': 'thing'})
    #remove promoted link on index 1
    articles.pop(0)
    for article in articles:
        try:
            unique_id = article['data-fullname'][3:]
            votes = article.find('div', {'class': 'score unvoted'}).text
            #if no one has voted yet, set votes to zero
            if not votes.isdigit():
                votes = 0
            link = article.find('a', {'class': 'title'})
            url = link['href']
            #dont save images and pdfs
            if _is_invalid_filetype(url) or _is_blacklisted_url(url):
                continue

            title = link.text
            comments_link = article.find('a', {'class': 'comments'})
            # if text start with comment no one has commented yet
            if comments_link.text.startswith('comment'):
                comments = 0
            else:
                comments = comments_link.text.split()[0]

            html = _fetch_html(url)
            zipped_html = _zip_html(html) if html else None

            posts.append(dict(
                unique_id=unique_id,
                url=url,
                html=zipped_html,
                title=title,
                votes=int(votes),
                comments=int(comments),
            ))
        except Exception:
            log.getFileLogger().exception('Could not parse article: %s' % (article,))

    return posts


def crawl_hackernews():
    """Fetch html from hackernews and parse data

        Fetches html from hackernews and parses the individual posts.
        Each post is represented by a dictionary, which has the following
        keys:
        unique_id: a string with the unique_id
        link: link of the item
        title: title of the item
        votes: number of votes
        comments: number of comments

        returns: a list of posts
    """
    try:
        request = requests.get(HN_URL)
        return _parse_hackernews(request.text)
    except:
        log.getFileLogger().exception(
            'couldnt parse hackernews, falling back to ihackernews api'
        )

    request = requests.get(IHACKERNEWS_URL)
    return _parse_ihackernews_api(request.json())


def crawl_reddit():
    """Fetch html from reddit/r/programming and parse data

        Fetches html from reddit and parses the individual posts.
        Each post is represented by a dictionary, which has the following
        keys:
        unique_id: a string with the unique_id
        link: link of the item
        title: title of the item
        votes: number of votes
        comments: number of comments

        returns: a list of posts
    """
    request = requests.get(REDDIT_URL)
    return _parse_reddit(request.text)


def fetch_html_for_posts(links):
    """Fetch html for the given links and returns them as set

        Needs links as a tuple - because of the fact that the
        database most likely will return this data structure.
        Returns a list containing sets because it will probably
        be inserted in the
        datebase again.
    """
    links_list = []

    for link in links:
        try:
            link_dict = {}

            html = _fetch_html(link['url'])
            zipped_html = _zip_html(html) if html else None

            link_dict['id'] = link['id']
            link_dict['html'] = zipped_html
            link_dict['url'] = link['url']
            links_list.append(link_dict)
        except:
            log.getFileLogger().exception('Error while processing: %s' % link['id'])

    return links_list
